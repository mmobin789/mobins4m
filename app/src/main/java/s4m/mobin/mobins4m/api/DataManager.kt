package s4m.mobin.mobins4m.api

import android.util.Log
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import s4m.mobin.mobins4m.models.Dictionary
import java.util.concurrent.TimeUnit

/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
class DataManager private constructor() {

    companion object {

        private var dataManager: DataManager? = null
        fun getInstance(): DataManager {
            if (dataManager == null)
                dataManager = DataManager()
            return dataManager!!
        }
    }

    private var retrofit: Retrofit? = null

    private fun getAPI(): API {

        if (retrofit == null) {
            val baseURL = "http://94.56.199.34/"
            val client = OkHttpClient().newBuilder().readTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS).build()


            retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(baseURL)
                    .client(client)
                    .build()
        }
        return retrofit!!.create(API::class.java)


    }

    fun loadData(url: String, onDictionaryListener: OnDictionaryListener) {
        getAPI().loadDictionary(url).enqueue(object : Callback<List<Dictionary>> {
            override fun onFailure(call: Call<List<Dictionary>>?, t: Throwable?) {
                Log.e("DictionaryAPI", t.toString())
            }

            override fun onResponse(call: Call<List<Dictionary>>?, response: Response<List<Dictionary>>?) {
                if (response!!.isSuccessful) {
                    val data = response.body()

                    Log.i("DictionaryAPI", Gson().toJson(data))
                    onDictionaryListener.onDictionaryReady(data!!)
                }
            }
        })
    }

}