package s4m.mobin.mobins4m.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url
import s4m.mobin.mobins4m.models.Dictionary

/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
interface API {
    @GET
    fun loadDictionary(@Url url: String): Call<List<Dictionary>>
}