package s4m.mobin.mobins4m.api

import s4m.mobin.mobins4m.models.Dictionary

/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
interface OnDictionaryListener {
    fun onDictionaryReady(dictionary: List<Dictionary>)
}