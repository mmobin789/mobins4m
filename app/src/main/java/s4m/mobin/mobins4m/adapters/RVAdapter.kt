package s4m.mobin.mobins4m.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_ui.*
import s4m.mobin.mobins4m.GlideApp
import s4m.mobin.mobins4m.R
import s4m.mobin.mobins4m.models.Dictionary

/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
class RVAdapter(private val onItemClickListener: OnItemClickListener, private val dictionary: List<Dictionary>) : RecyclerView.Adapter<RVAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_ui, parent, false))
    }

    override fun getItemCount(): Int {
        return dictionary.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val title = dictionary[position].aNNOUNCEMENTTITLE
        val image = dictionary[position].aNNOUNCEMENTIMAGE
        holder.tv.text = title.value
        if (image.value.isNotBlank()) {
            val url = "http" + image.value.substring(4, image.value.length)
            Log.i("imageURL", url)
            GlideApp.with(holder.containerView).load(url).placeholder(R.drawable.ic_launcher_background).into(holder.iv)
        }
    }


    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        init {
            containerView.setOnClickListener {
                onItemClickListener.onItemClick(adapterPosition)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }


}