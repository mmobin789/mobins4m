package s4m.mobin.mobins4m.models

import com.google.gson.annotations.SerializedName


data class Dictionary(

        @field:SerializedName("ANNOUNCEMENT_DATE")
        val aNNOUNCEMENTDATE: AnnouncementDate,

        @field:SerializedName("ANNOUNCEMENT_TITLE")
        val aNNOUNCEMENTTITLE: AnnouncementTitle,

        @field:SerializedName("ANNOUNCEMENT_HTML")
        val aNNOUNCEMENTHTML: AnnouncementHtml,

        @field:SerializedName("ANNOUNCEMENT_IMAGE_THUMBNAIL")
        val aNNOUNCEMENTIMAGETHUMBNAIL: AnnouncementImageThumbnail,

        @field:SerializedName("Expiry")
        val eXPIRY: Expiry? = null,

        @field:SerializedName("NATIVE_DATE")
        val nATIVEDATE: NativeDate? = null,

        @field:SerializedName("ANNOUNCEMENT_IMAGE")
        val aNNOUNCEMENTIMAGE: AnnouncementImage,

        @field:SerializedName("ID")
        val iD: ID? = null,

        @field:SerializedName("ANNOUNCEMENT_DESCRIPTION")
        val aNNOUNCEMENTDESCRIPTION: AnnouncementDescription? = null
)