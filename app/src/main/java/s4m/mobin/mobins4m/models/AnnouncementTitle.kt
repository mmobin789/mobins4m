package s4m.mobin.mobins4m.models

import com.google.gson.annotations.SerializedName


data class AnnouncementTitle(

        @field:SerializedName("TypeCode")
        val typeCode: Int,

        @field:SerializedName("Value")
        val value: String,

        @field:SerializedName("IsBinaryUnique")
        val isBinaryUnique: Boolean,

        @field:SerializedName("Tag")
        val tag: String
)