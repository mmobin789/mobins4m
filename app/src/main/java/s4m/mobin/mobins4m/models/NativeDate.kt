package s4m.mobin.mobins4m.models

import com.google.gson.annotations.SerializedName


data class NativeDate(

        @field:SerializedName("TypeCode")
        val typeCode: Int? = null,

        @field:SerializedName("Value")
        val value: String? = null,

        @field:SerializedName("IsBinaryUnique")
        val isBinaryUnique: Boolean? = null,

        @field:SerializedName("Tag")
        val tag: String? = null
)