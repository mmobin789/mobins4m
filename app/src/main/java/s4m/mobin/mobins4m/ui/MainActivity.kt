package s4m.mobin.mobins4m.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import s4m.mobin.mobins4m.R
import s4m.mobin.mobins4m.adapters.RVAdapter
import s4m.mobin.mobins4m.api.DataManager
import s4m.mobin.mobins4m.api.OnDictionaryListener
import s4m.mobin.mobins4m.models.Dictionary

class MainActivity : AppCompatActivity(), OnDictionaryListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rv.layoutManager = LinearLayoutManager(this)
        getDictionary()
    }

    private fun getDictionary() {
        rv.visibility = View.GONE
        val manager = DataManager.getInstance()
        val url = "http://94.56.199.34/EMC/IPDP/ipdpb.ashx?TemplateName=Promotions_ipad.htm&p=Common.Announcements&Handler=News&AppName=EMC&Type=News&F=J"
        manager.loadData(url, this)
    }

    override fun onDictionaryReady(dictionary: List<Dictionary>) {
        rv.visibility = View.VISIBLE
        loader.smoothToHide()
        Log.i("dictionarySize", dictionary.size.toString())

        rv.adapter = RVAdapter(object : RVAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                val html = dictionary[position].aNNOUNCEMENTHTML
                val detail = Intent(this@MainActivity, Detail::class.java)
                detail.putExtra("html", html.value)
                startActivity(detail)
            }
        }, dictionary)

    }


}
