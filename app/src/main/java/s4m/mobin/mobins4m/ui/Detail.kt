package s4m.mobin.mobins4m.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail.*
import s4m.mobin.mobins4m.R

class Detail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        wv.loadData(intent.getStringExtra("html"), "text/html", "UTF-8")
    }
}
